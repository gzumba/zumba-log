<?php

namespace Zumba\Log;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

interface LoggingI extends LoggerAwareInterface
{
    public function setLogger(LoggerInterface $logger = null, $channel = null);

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger();

    public function setChannel($channel);

    public function hasLogger();
}
