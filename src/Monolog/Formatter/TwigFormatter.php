<?php

namespace Zumba\Log\Monolog\Formatter;

use Monolog\Formatter\FormatterInterface;

class TwigFormatter implements FormatterInterface
{
    protected $twig;
    protected $record_template_name;
    protected $batch_template_name;

    public function __construct(\Twig_Environment $twig, $record_template_name, $batch_template_name)
    {
        $this->twig = $twig;
        $this->record_template_name = $record_template_name;
        $this->batch_template_name = $batch_template_name;
    }

    /**
     * {@inheritdoc}
     */
    public function format(array $record, $loop_num = 1)
    {
        $record['log_loop_num'] = $loop_num;

        return $this->twig->render($this->record_template_name, $record);
    }

    public function formatBatch(array $records)
    {
        $messages = [];

        $loop_num = 1;
        foreach ($records as $record) {
            $messages[] = $this->format($record, $loop_num);
            $loop_num++;
        }

        return $this->twig->render($this->batch_template_name, ['messages' => $messages]);
    }
}
