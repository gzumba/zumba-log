<?php

namespace Zumba\Log\Monolog\Processor;

class ChannelProcessor
{
    public function __construct()
    {
    }

    /**
     * @param  array $record
     * @return array
     */
    public function __invoke(array $record)
    {
        if (isset($record['context']['channel'])) {
            $record['channel'] = $record['context']['channel'];
            unset($record['context']['channel']);
        }

        return $record;
    }
}
