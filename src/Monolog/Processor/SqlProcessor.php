<?php

namespace Zumba\Log\Monolog\Processor;

/**
 * Formats any possible SQL entry in the context of record
 */
class SqlProcessor
{
    protected $sql_key;
    protected $format_as_html;

    public function __construct($format_as_html = false, $sql_key = 'sql')
    {
        $this->format_as_html = $format_as_html;
        $this->sql_key = $sql_key;
    }

    /**
     * @param array $record
     * @return array
     */
    public function __invoke(array $record)
    {
        if (!isset($record['context'][$this->sql_key])) {
            return $record;
        }

        $record['context']["{$this->sql_key}_formatted"] =
            \SqlFormatter::format($record['context'][$this->sql_key], $this->format_as_html);
        unset($record['context'][$this->sql_key]);

        return $record;
    }
}
