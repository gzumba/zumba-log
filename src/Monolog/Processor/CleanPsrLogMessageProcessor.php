<?php

namespace Zumba\Log\Monolog\Processor;

/**
 * Processes a record's message according to PSR-3 rules, also strip newlines from message
 *
 * It replaces {foo} with the value from $context['foo'] removing it at the same time
 */
class CleanPsrLogMessageProcessor
{
    /**
     * @param array $record
     * @return array
     */
    public function __invoke(array $record)
    {
        if (false === strpos($record['message'], '{')) {
            return $record;
        }

        $replacements = [];
        foreach ($record['context'] as $key => $val) {
            if (false !== strpos($record['message'], "{{$key}}")) {
                $replacements['{' . $key . '}'] = $this->stringifyVal($val);
                unset($record['context'][$key]);
            }
        }

        $record['message'] = strtr($record['message'], $replacements);

        $record['message'] = str_replace(["\n", "\r"], [' ', ' '], $record['message']);

        return $record;
    }

    private function stringifyVal($val): string
    {
        if (is_null($val) || is_scalar($val) || (is_object($val) && method_exists($val, "__toString"))) {
            return (string) $val;
        }

        if (is_array($val)) {
            return sprintf("[%s]", implode(', ', array_map(fn($element) => $this->stringifyVal($element), $val)));
        }

        if (is_object($val)) {
            return sprintf("[object %s]", get_class($val));
        }
        
        return '[' . gettype($val) . ']';
    }
}
