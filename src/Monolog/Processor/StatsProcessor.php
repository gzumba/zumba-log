<?php

namespace Zumba\Log\Monolog\Processor;

use Zumba\Commons\StatsableI;

class StatsProcessor
{
    private $statsables = [];

    public function addStatsable(StatsableI $statsable)
    {
        $this->statsables[] = $statsable;
    }

    /**
     * @param  array $record
     * @return array
     */
    public function __invoke(array $record)
    {
        foreach ($this->statsables as $statsable) {
            $record['extra'] = array_merge(
                $record['extra'],
                $statsable->getStats()
            );
        }

        // FIXME: with PHP5.4 we have this available from apache
        if (isset($GLOBALS['request_start_time'])) {
            $record['extra']['time'] = sprintf("%.3f", microtime(true) - $GLOBALS['request_start_time']);
        }

        return $record;
    }
}
