<?php
declare(strict_types=1);

namespace Zumba\Log;

use Psr\Log\LoggerInterface;

trait LoggingTrait
{
    protected ?LoggerInterface $logger = null;
    protected ?string $channel = null;

    /** @required */
    public function setLogger(LoggerInterface $logger = null, $channel = null): void
    {
        $this->logger = $logger;
        if ($channel) {
            $this->channel = $channel;
        }
    }

    public function getLogger(): ?LoggerInterface
    {
        return $this->logger;
    }

    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    public function hasLogger(): bool
    {
        return $this->logger !== null;
    }

    protected function logAlert($message, array $context = [])
    {
        if ($this->logger === null) {
            return;
        }

        if ($this->channel) {
            $context['channel'] = $this->channel;
        }

        $this->logger->alert($message, $context);
    }

    /**
     * @deprecated Use ::logCritical instead
     */
    protected function logCrit($message, array $context = [])
    {
        $this->logCritical($message, $context);
    }

    protected function logCritical($message, array $context = [])
    {
        if ($this->logger === null) {
            return;
        }
        if ($this->channel) {
            $context['channel'] = $this->channel;
        }
        $this->logger->critical($message, $context);
    }

    protected function logDebug($message, array $context = [])
    {
        if ($this->logger === null) {
            return;
        }
        if ($this->channel) {
            $context['channel'] = $this->channel;
        }
        $this->logger->debug($message, $context);
    }

    /**
     * @deprecated use ::logEmergency instead
     */
    protected function logEmerg($message, array $context = [])
    {
        $this->logEmergency($message, $context);
    }

    protected function logEmergency($message, array $context = [])
    {
        if ($this->logger === null) {
            return;
        }
        if ($this->channel) {
            $context['channel'] = $this->channel;
        }
        $this->logger->emergency($message, $context);
    }

    /**
     * @deprecated use ::logError instead
     */
    protected function logErr($message, array $context = [])
    {
        $this->logError($message, $context);
    }

    protected function logError($message, array $context = [])
    {
        if ($this->logger === null) {
            return;
        }
        if ($this->channel) {
            $context['channel'] = $this->channel;
        }
        $this->logger->error($message, $context);
    }

    protected function logInfo($message, array $context = [])
    {
        if ($this->logger === null) {
            return;
        }
        if ($this->channel) {
            $context['channel'] = $this->channel;
        }
        $this->logger->info($message, $context);
    }

    protected function logNotice($message, array $context = [])
    {
        if ($this->logger === null) {
            return;
        }
        if ($this->channel) {
            $context['channel'] = $this->channel;
        }
        $this->logger->notice($message, $context);
    }

    /**
     * @deprecated use ::logWarning instead
     */
    protected function logWarn($message, array $context = [])
    {
        $this->logWarning($message, $context);
    }

    protected function logWarning($message, array $context = [])
    {
        if ($this->logger === null) {
            return;
        }
        if ($this->channel) {
            $context['channel'] = $this->channel;
        }
        $this->logger->warning($message, $context);
    }
}
