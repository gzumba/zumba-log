<?php

namespace Zumba\Tests\Monolog\Processor;

use PHPUnit\Framework\TestCase;
use Zumba\Log\Monolog\Processor\CleanPsrLogMessageProcessor;

class CleanPsrLogMessageProcessorTest extends TestCase
{
    private CleanPsrLogMessageProcessor $processor;

    public function setUp(): void
    {
        parent::setUp();
        $this->processor  = new CleanPsrLogMessageProcessor();
    }

    public function testRemovesUsedOnly(): void
    {
        $record = ['message' => 'foo {sql} bar', 'context' => ['sql' => 'replaced', 'not' => 'replaced']];

        $newrecord = $this->processor->__invoke($record);

        self::assertEquals("foo replaced bar", $newrecord['message']);
        self::assertArrayNotHasKey('sql', $newrecord['context']);
        self::assertArrayHasKey('not', $newrecord['context']);
    }

    public function testDoNotCleanPartialWords(): void
    {
        $record = ['message' => 'foo {sql bar', 'context' => ['sql' => 'notreplaced', 'not' => 'replaced']];

        $newrecord = $this->processor->__invoke($record);

        self::assertEquals("foo {sql bar", $newrecord['message']);
        self::assertArrayHasKey('sql', $newrecord['context']);
    }

    public function testWorksWithArrays(): void
    {
        $record = ['message' => 'foo {array} bar', 'context' => ['array' => [1, 2]]];

        $newrecord = $this->processor->__invoke($record);

        self::assertEquals("foo [1, 2] bar", $newrecord['message']);
    }

}
