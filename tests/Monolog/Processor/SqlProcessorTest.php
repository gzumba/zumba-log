<?php

namespace Zumba\Tests\Monolog\Processor;

use PHPUnit\Framework\TestCase;
use Zumba\Log\Monolog\Processor\SqlProcessor;

class SqlProcessorTest extends TestCase
{
	public function testConstruct()
	{
		$processor = new SqlProcessor(false,'testkey');

		return $processor;
	}

	/**
	 * @depends testConstruct
	 */
	public function testNothingIfNoKey(SqlProcessor $processor)
	{
		$record = array('message'=>'foo','context'=>array('sql'=>'SELECT * FROM table'));

		$newrecord = $processor($record);

		$this->assertEquals($record, $newrecord);
	}

	/**
	 * @depends testConstruct
	 */
	public function testSqlProcessing(SqlProcessor $processor)
	{
		$record = array('context'=>array('testkey'=>'SELECT * FROM table'));

		$newrecord = $processor($record);
		$this->assertEquals("SELECT \n  * \nFROM \n  table", $newrecord['context']['testkey_formatted']);
		$this->assertFalse(isset($newrecord['context']['testkey']));
	}

}
