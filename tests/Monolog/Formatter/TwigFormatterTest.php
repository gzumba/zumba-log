<?php

namespace Zumba\Tests\Monolog\Formatter;

use PHPUnit\Framework\TestCase;
use Zumba\Log\Monolog\Formatter\TwigFormatter;

class TwigFormatterTest extends TestCase
{
	public function testConstruct()
	{
		$twig = new \Twig_Environment();
		$loader = new \Twig_Loader_String();
		$twig->setLoader($loader);
		$batch_template = "{% for message in messages %}{{ message }}{% endfor %}";
		$record_template = "{{ level_name }}";
		$formatter = new TwigFormatter($twig, $record_template, $batch_template);

		return $formatter;
	}

	/**
	 * @depends testConstruct
	 */
	public function testFormat(TwigFormatter $formatter)
	{
		$message = $formatter->format(array(
			'level_name' => 'WARNING',
			'channel' => 'log',
			'context' => array(),
			'message' => 'foo',
			'datetime' => new \DateTime,
			'extra' => array(),
		));

		$this->assertEquals("WARNING",$message);

		return $formatter;
	}

	/**
	 * @depends testFormat
	 */
	public function testBatchFormat(TwigFormatter $formatter)
	{
		$message = $formatter->formatBatch(array(
				array('level_name'=>'WARNING'),
				array('level_name'=>'ERROR'),
			)
		);
		$this->assertEquals("WARNINGERROR", $message);
	}
}
