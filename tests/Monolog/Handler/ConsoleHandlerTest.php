<?php

namespace Zumba\Tests\Monolog\Handler;


use PHPUnit\Framework\TestCase;
use Zumba\Log\Monolog\Handler\ConsoleHandler;

class ConsoleHandlerTest extends TestCase
{
	public function testConstruct()
	{
		$handler = new ConsoleHandler();

		return $handler;
	}

	/**
	 * @depends testConstruct
	 */
	public function testDisable(ConsoleHandler $handler)
	{
		$handler->disable();
		$this->assertFalse($handler->handle([]));
		return $handler;
	}

}
