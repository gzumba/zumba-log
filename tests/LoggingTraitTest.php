<?php

namespace Zumba\Log\Tests;

use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;

use Monolog\Logger;

use Monolog\Formatter\LineFormatter;

use Monolog\Handler\TestHandler;
use Zumba\Log\Monolog\Processor\CleanPsrLogMessageProcessor;

class LoggingTraitTest extends TestCase
{
    private TestHandler $handler;

    public function getLogger()
    {
        $logger = new Logger('name');
        $logger->pushHandler($handler = new TestHandler);
        $logger->pushProcessor(new CleanPsrLogMessageProcessor());
        $handler->setFormatter(new LineFormatter('%level_name% %message%'));

        $this->handler = $handler;

        $loggable = new TraitLoggable();
        $loggable->setLogger($logger, 'channel');

        return $loggable;
    }

    public function getLogs(): array
    {
        $convert = function ($record) {
            $lower = function ($match) {
                return strtolower($match[0]);
            };

            return preg_replace_callback('{^[A-Z]+}', $lower, $record['formatted']);
        };

        return array_map($convert, $this->handler->getRecords());
    }

    public function testHasLoggerWorks()
    {
        $logger = $this->getLogger();
        self::assertTrue($logger->hasLogger());

        $logger->setLogger(null);
        self::assertFalse($logger->hasLogger());
    }

    /**
     * @dataProvider provideLoggableLevelsAndMessages
     */
    public function testTryLogsAtAllLevels($level_name, $method, $message)
    {
        $logger = $this->getLogger();
        $method_name = "log{$method}";
        $logger->callLogMethod($method_name, $message, ['user' => 'Bob']);

        $expected = array(
            $level_name . ' message of level ' . $level_name . ' with context: Bob',
        );

        $this->assertEquals($expected, $this->getLogs());
        $logger->setLogger(null);
        // verify the method works without a logger
        $logger->callLogMethod($method_name, $message, ['user' => 'Bob']);
    }

    public function provideLoggableLevelsAndMessages(): array
    {
        return [
            LogLevel::EMERGENCY => [
                LogLevel::EMERGENCY,
                'Emergency',
                'message of level emergency with context: {user}'
            ],
            LogLevel::ALERT => [LogLevel::ALERT, 'Alert', 'message of level alert with context: {user}'],
            LogLevel::CRITICAL => [LogLevel::CRITICAL, 'Critical', 'message of level critical with context: {user}'],
            LogLevel::ERROR => [LogLevel::ERROR, 'Error', 'message of level error with context: {user}'],
            LogLevel::WARNING => [LogLevel::WARNING, 'Warning', 'message of level warning with context: {user}'],
            LogLevel::NOTICE => [LogLevel::NOTICE, 'Notice', 'message of level notice with context: {user}'],
            LogLevel::INFO => [LogLevel::INFO, 'Info', 'message of level info with context: {user}'],
            LogLevel::DEBUG => [LogLevel::DEBUG, 'Debug', 'message of level debug with context: {user}'],
        ];
    }

}
