<?php

namespace Zumba\Log\Tests;

use Zumba\Log\LoggingI;
use Zumba\Log\LoggingTrait;

class TraitLoggable implements LoggingI
{
	use LoggingTrait;

  public function callLogMethod($method, $message, $context): void
  {
      $this->{$method}($message, $context);
  }
}
